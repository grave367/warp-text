# Warp Text



## What this does

I͗ w̌a̟s͔ e̤̤x̍̍pͨͨl̨̨a̾̾ȉ̏n͒͒i͙͙n̷̷g̊̊ a̢̢sͅͅc͓͓i͝͝i̴̴ i̕̕̕n̘̘̘j̙̙̙eͣͣͣcͣͣͣt̿̿̿i͝͝͝o͐͐͐n̕̕̕ t͖͖͖o͗͗͗ aͬͬͬͬ c͞͞͞͞o̵̵̵̵wͫͫͫͫo̾̾̾̾r̍̍̍̍k͆͆͆͆e̅̅̅̅r͎͎͎͎ a͎͎͎͎͎n͈͈͈͈͈d̕̕̕̕̕ w̯̯̯̯̯a̹̹̹̹̹n͐͐͐͐͐t̍̍̍̍̍e͟͟͟͟͟d̚̚̚̚̚ t͐͐͐͐͐͐o̱̱̱̱̱̱ d̽̽̽̽̽̽e̿̿̿̿̿̿mͣͣͣͣͣͣo͞͞͞͞͞͞n̟̟̟̟̟̟s̺̺̺̺̺̺t̠̠̠̠̠̠r̶̶̶̶̶̶a̩̩̩̩̩̩t̰̰̰̰̰̰e̖̖̖̖̖̖ i͡͡͡͡͡͡͡t͔͔͔͔͔͔͔, t͈͈͈͈͈͈͈ḧ̈̈̈̈̈̈i̠̠̠̠̠̠̠sͪͪͪͪͪͪͪ c̼̼̼̼̼̼̼̼oͯͯͯͯͯͯͯͯd̼̼̼̼̼̼̼̼e͚͚͚͚͚͚͚͚ ḯ̈́̈́̈́̈́̈́̈́̈́n̛̛̛̛̛̛̛̛j̈́̈́̈́̈́̈́̈́̈́̈́e̸̸̸̸̸̸̸̸c̗̗̗̗̗̗̗̗t̖̖̖̖̖̖̖̖s̨̨̨̨̨̨̨̨ a̅̅̅̅̅̅̅̅̅t̚̚̚̚̚̚̚̚̚y̪̪̪̪̪̪̪̪̪p̞̞̞̞̞̞̞̞̞i͒͒͒͒͒͒͒͒͒c͘͘͘͘͘͘͘͘͘a͉͉͉͉͉͉͉͉͉lͥͥͥͥͥͥͥͥͥ à̀̀̀̀̀̀̀̀s͋͋͋͋͋͋͋͋͋c͑͑͑͑͑͑͑͑͑iͯͯͯͯͯͯͯͯͯi̓̓̓̓̓̓̓̓̓ t̻̻̻̻̻̻̻̻̻̻e̎̎̎̎̎̎̎̎̎̎x͍͍͍͍͍͍͍͍͍͍t̕̕̕̕̕̕̕̕̕̕ toͣͣͣͣͣͣͣͣͣͣ g̩̩̩̩̩̩̩̩̩̩̩i̳̳̳̳̳̳̳̳̳̳̳v̼̼̼̼̼̼̼̼̼̼̼eͫͫͫͫͫͫͫͫͫͫͫ a͇͇͇͇͇͇͇͇͇͇͇ s͒͒͒͒͒͒͒͒͒͒͒͒t̤̤̤̤̤̤̤̤̤̤̤̤y͝͝͝͝͝͝͝͝͝͝͝͝l̎̎̎̎̎̎̎̎̎̎̎̎i̽̽̽̽̽̽̽̽̽̽̽̽s̐̐̐̐̐̐̐̐̐̐̐̐t̐̐̐̐̐̐̐̐̐̐̐̐i̛̛̛̛̛̛̛̛̛̛̛̛cͅͅͅͅͅͅͅͅͅͅͅͅ o̘̘̘̘̘̘̘̘̘̘̘̘ù̀̀̀̀̀̀̀̀̀̀̀t̲̲̲̲̲̲̲̲̲̲̲̲p͇͇͇͇͇͇͇͇͇͇͇͇u̴̴̴̴̴̴̴̴̴̴̴̴t̽̽̽̽̽̽̽̽̽̽̽̽

## What this does (normal)

I was explaining ascii injection to a coworker and wanted to demonstrate it, this code injects atypical ascii text to give a stylistic output

## How to use
Edit generate.py's last line
The line
'''
deform('EDIT-THIS-TEXT')
'''

Editing this text will change the output from 'EDIT-THIS-TEXT' to 'E̵D̀I͂T̟-T̓ḢIͅS̨-T̄E̕X̣T̬'

## Purposes
Mostly for fun, could be wrapped up in an interface, but this is mostly a toy that demonstrates atypical ascii injection, the text is typically still usable in most forms, but will have adverse display effects. I like doing a deformed username in multiplayer games using this.
