import random

def deform(text: str):
    marks = [chr(i) for i in range(768, 880)]
    words = text.split()

    deformed_words = []
    for i, word in enumerate(words):
        deformed_chars = []
        for c in word:
            if c.isalnum():
                mark = random.choice(marks)
                repetition = (i // 2 + 1) * c.isalnum()
                deformed_chars.append(c + mark * repetition)
            else:
                deformed_chars.append(c)
        deformed_word = ''.join(deformed_chars)
        deformed_words.append(deformed_word)

    deformed_text = ' '.join(deformed_words)
    print(deformed_text)

deform('EDIT-THIS-TEXT')
